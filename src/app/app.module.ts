import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { JsonComponent } from './json/json.component';
import { HeaderComponent } from './header/header.component';
import { FormComponent } from './form/form.component';
import { RupeePipe } from './rupee.pipe';
import { AppRoutingModule } from './form/app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    JsonComponent,
    HeaderComponent,
    FormComponent,
    RupeePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
