import {  Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormComponent } from '../form/form.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-json',
  templateUrl: './json.component.html',
  styleUrls: ['./json.component.css']
})
export class JsonComponent implements OnInit{
  json:any;
  
  constructor(private route: Router,private routes:ActivatedRoute){

  }

  ngOnInit(): void {
    this.routes.queryParams.subscribe((params)=>{
      console.log(params);
      this.json=JSON.parse(params['data']);
        })
  }
  
}
