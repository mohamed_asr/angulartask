import { Component, EventEmitter, Output} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  currency='';
  // @ViewChild('age')ageInput:ElementRef;
  
 

  constructor(private route: Router,private routes:ActivatedRoute){

  }
  onClick(form:NgForm){
    let data:any;
    data=form.value;
    console.log(data);
    this.route.navigate(['./json'],{
      queryParams:{data:JSON.stringify(data)}
    });
  }

 
  
  
   
}
