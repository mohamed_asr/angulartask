import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { JsonComponent } from "../json/json.component";
import { FormComponent } from "./form.component";

const appRoutes: Routes=[
    {path:'',redirectTo:'/form',pathMatch:"full"},
    {path:'form',component:FormComponent},
    {path:'json',component:JsonComponent}
]

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes)
    ],
    exports:[
        RouterModule
    ]
})



export class AppRoutingModule {

}