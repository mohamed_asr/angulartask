import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rupee'
})
export class RupeePipe implements PipeTransform {

  transform(value: any):any {
    if(value==null){
      return null;
    }
    else
    return '₹'+value*82.37;
  }

}
